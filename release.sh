#!/bin/bash

CURR="released"
NEXT="next_release"
PREV="prev_release"

if [ "$1" == "--rollback" ]
then
    if [ -e "$PREV" ]
    then
        echo "Rolling back to $PREV"
        rm $NEXT
        mv -T $PREV $CURR
    else
        echo "No previous release"
    fi

    exit 0
fi

if [ "$1" == "--force" ]
then
    do_release=0
else
    # not modified in the last hour
    test `find "$NEXT" -mmin +60`
    do_release=$?
fi

if [ $do_release == 0 ] && [ -e "$NEXT" ]
then
    echo "Releasing"
    mv -T $CURR $PREV
    mv -T $NEXT $CURR
else
    echo "Not ready to release"
fi
