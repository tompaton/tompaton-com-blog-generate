#!/usr/bin/env python
# coding: utf-8

import argparse, json, os, re, subprocess, sys, traceback
from pathlib import Path
from pprint import pprint
from datetime import datetime
import dateutil.tz
from slugify import slugify

from mako_filters import clean

def _load():
    return json.load(open(os.environ['CONTENT_FILE'], 'r'))

def _save(json_feed):
    json.dump(json_feed,
              open(os.environ['CONTENT_FILE'], 'w'),
              sort_keys=True,
              indent=4)

def list_posts(args):
    json_feed = _load()
    print(json_feed['title'])
    for i, item in enumerate(json_feed['items']):
        print(i, item['id'])

def show_post(args):
    json_feed = _load()
    item = json_feed['items'][args.item]

    for content_type in ('content_text', 'content_html'):
        if content_type in item:
            content = item.pop(content_type)
            break

    pprint(item)

    if args.dump:
        print("Wrote {} to {}.".format(content_type, args.dump.name))
        print(content, file=args.dump)
    else:
        print(content_type)
        print(content)

def add_bookmark(args):
    json_feed = _load()

    ts = datetime.now(dateutil.tz.tzlocal()).replace(microsecond=0)
    url = '{}{}/bookmark-{}'.format(json_feed['home_page_url'],
                                    ts.strftime('%Y/%m/%d'),
                                    slugify(args.title))

    item = {'content_text': args.content_text or args.external_url,
            'date_published': ts.isoformat(),
            'external_url': args.external_url,
            'id': url,
            'tags': ['bookmarks'],
            'title': args.title,
            'url': url}

    json_feed['items'].insert(0, item)

    _save(json_feed)

def add_podcast(args):
    json_feed = _load()

    ts = datetime.now(dateutil.tz.tzlocal()).replace(microsecond=0)
    url = '{}{}/podcast-{}'.format(json_feed['home_page_url'],
                                   ts.strftime('%Y/%m/%d'),
                                   slugify(args.title))

    item = {'content_text': args.content_text or args.external_url,
            'attachments': [{'url': args.attachment, '_rel': 'audio'}],
            'date_published': ts.isoformat(),
            'external_url': args.external_url,
            'id': url,
            'tags': ['podcast'],
            'title': args.title,
            'url': url}

    json_feed['items'].insert(0, item)

    _save(json_feed)

def add_post(args):
    json_feed = _load()

    ts = datetime.now(dateutil.tz.tzlocal()).replace(microsecond=0)
    url = '{}{}/{}'.format(json_feed['home_page_url'],
                           ts.strftime('%Y/%m/%d'),
                           slugify(args.title))

    item = {'date_published': ts.isoformat(),
            'id': url,
            'tags': ['posts'],
            'title': args.title,
            'url': url}

    if args.content_html:
        item['content_html'] = args.content_html.read()

    if args.content_text:
        item['content_text'] = args.content_text.read()

    json_feed['items'].insert(0, item)

    _save(json_feed)

def edit_post(args):
    json_feed = _load()
    item = json_feed['items'][args.item]

    def edit(key, value):
        print('Updated {}: {} --> {}'.format(key, item[key], value))
        item[key] = value

    for key in ('id', 'url', 'external_url', 'date_published', 'title', 'tags'):
        if getattr(args, key):
            edit(key, getattr(args, key))

    if args.tag:
        print('Appended tags: {}'.format(args.tag))
        for tag in args.tag:
            if tag not in item['tags']:
                item['tags'].append(tag)

    if args.content_html:
        print('Updated content_html.')
        item['content_html'] = args.content_html.read()
        item.pop('content_text', None)

    if args.content_text:
        print('Updated content_text.')
        item['content_text'] = args.content_text.read()
        item.pop('content_html', None)

    if args.attachment:
        print('Added attachment.')
        if 'attachments' not in item:
            item['attachments'] = []
        item['attachments'].append({'url': args.attachment})

    if args.via:
        print('Added via.')
        _force_content_html(item)
        item['content_html'] \
            += '<p>via <a href="{1}">{0}</a></p>'.format(*args.via)

    if args.image:
        print('Added image.')

        cmd = ['identify', '-format', '%m', args.image.name]
        ext = '.' + subprocess.check_output(cmd).decode("utf-8").lower().strip()

        image = (Path('content')
                 / item['url'].replace(json_feed['home_page_url'], '')
                 / 'image').with_suffix(ext)
        thumb = image.with_name('thumb' + ext)

        content_dir = Path(os.environ['CONTENT_DIR'])

        print(image)
        print(thumb)

        image_filename = content_dir / image
        os.makedirs(str(image_filename.parent), exist_ok=True)
        with image_filename.open('wb') as output:
            output.write(args.image.read())

        # create thumbnail
        thumb_filename = content_dir / thumb
        cmd = ['convert',
               '-strip',
               '-thumbnail',
               '600x>',
               str(image_filename),
               str(thumb_filename)]
        subprocess.check_call(cmd)

        item['image'] = json_feed['home_page_url'] + str(image)
        item['_image_thumb'] = json_feed['home_page_url'] + str(thumb)

    _save(json_feed)

def _force_content_html(item):
    if 'content_text' in item:
        item['content_html'] \
            = '<p>{}</p>'.format(clean(item.pop('content_text')))

def delete_post(args):
    json_feed = _load()
    item = json_feed['items'][args.item]

    print('Deleted {}'.format(item['id']))

    del json_feed['items'][args.item]

    _save(json_feed)

def cache_post(args):
    json_feed = _load()
    item = json_feed['items'][args.item]

    cache = os.environ['CACHE_DIR']
    url = item.get('external_url')

    if not url:
        print('[{}] No external url.'.format(args.item))
        return

    if any(attachment.get('title') == 'cached'
           for attachment in item.get('attachments', [])):
        if args.overwrite:
            print('[{}] Already cached - overwriting...'.format(args.item))
        else:
            print('[{}] Already cached.'.format(args.item))
            return

    try:
        # TODO: wget 1.19.1 --> --retry-on-http-error=429
        cmd = ['wget',
               '--page-requisites',
               '--convert-links',
               '--adjust-extension',
               '-P', cache,
               url]

        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT) \
                           .decode("utf-8")

    except subprocess.CalledProcessError as e:
        print('[{}] wget error: {}'.format(args.item, e))

        output = e.output.decode("utf-8")
        if args.show_output:
            # traceback.print_exc(file=sys.stdout)
            print(output)

        request = None
        for line in output.splitlines():
            if line.startswith('--'):
                request = line
            if ' ERROR ' in line and request is not None:
                print('ERROR ' + line.split(' ERROR ')[1]
                      + ' ' + request.split('  ')[1])

        if not args.ignore_errors:
            return

    html = re.search('^Saving to: ‘(.+)’', output, re.MULTILINE)

    if html:
        saved = html.group(1).replace(cache, '')

        print('[{}] Saved {} to {}'.format(args.item, url, 'cached/' + saved))

        if 'attachments' not in item:
            item['attachments'] = []

        item['attachments'].append(
            {'title': 'cached',
             'url': 'cached/' + saved,
             '_rel': 'nofollow'})

        _save(json_feed)

    else:
        print('[{}] wget error: {}'.format(args.item, 'output path not found'))
        if args.show_output:
            print(output)


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest='func_name')

parser_list = subparsers.add_parser('list')
parser_list.set_defaults(func=list_posts)

parser_show = subparsers.add_parser('show')
parser_show.set_defaults(func=show_post)
parser_show.add_argument('item', type=int)
parser_show.add_argument('--dump', type=argparse.FileType('w'))

parser_bookmark = subparsers.add_parser('bookmark')
parser_bookmark.set_defaults(func=add_bookmark)
parser_bookmark.add_argument('external_url')
parser_bookmark.add_argument('title')
parser_bookmark.add_argument('--content_text')

parser_podcast = subparsers.add_parser('podcast')
parser_podcast.set_defaults(func=add_podcast)
parser_podcast.add_argument('external_url')
parser_podcast.add_argument('title')
parser_podcast.add_argument('attachment')
parser_podcast.add_argument('--content_text')

parser_post = subparsers.add_parser('post')
parser_post.set_defaults(func=add_post)
parser_post.add_argument('title')
parser_post.add_argument('--content_html', type=argparse.FileType('r'))
parser_post.add_argument('--content_text', type=argparse.FileType('r'))

parser_edit = subparsers.add_parser('edit')
parser_edit.set_defaults(func=edit_post)
parser_edit.add_argument('item', type=int)
parser_edit.add_argument('--id')
parser_edit.add_argument('--url')
parser_edit.add_argument('--external_url')
parser_edit.add_argument('--date_published')
parser_edit.add_argument('--title')
parser_edit.add_argument('--tags')
parser_edit.add_argument('--tag', action='append')
parser_edit.add_argument('--content_html', type=argparse.FileType('r'))
parser_edit.add_argument('--content_text', type=argparse.FileType('r'))
parser_edit.add_argument('--attachment')
parser_edit.add_argument('--via', nargs=2, help="NAME URL")
parser_edit.add_argument('--image', type=argparse.FileType('rb'))

parser_delete = subparsers.add_parser('delete')
parser_delete.set_defaults(func=delete_post)
parser_delete.add_argument('item', type=int)

parser_cache = subparsers.add_parser('cache')
parser_cache.set_defaults(func=cache_post)
parser_cache.add_argument('item', type=int)
parser_cache.add_argument('--overwrite', action='store_true')
parser_cache.add_argument('--show_output', action='store_true')
parser_cache.add_argument('--ignore_errors', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    if args.func_name:
        args.func(args)
    else:
        parser.print_help()
