#!/usr/bin/env python

import calendar
import contextlib
import json
import os
import shutil
import sys
from copy import deepcopy
from datetime import datetime

import dateutil.parser
import dateutil.tz
import bleach
from mako.template import Template

BLOG_CSS_VERSION = 1
BLOG_CSS = 'blog-%03d.css' % BLOG_CSS_VERSION

def process(posts):
    for item in posts['items']:
        item['date_published'] = dateutil.parser.parse(item['date_published'])
    return posts

def get_tags(posts):
    return {tag for item in posts['items']
            for tag in item.get('tags', [])}

def tag_posts(posts, tag):
    return [item for item in posts['items']
            if tag in item.get('tags', [])]

def public_tags(tags):
    """don't show @user tags on page"""
    return [('tags/' + tag, tag) for tag in tags
            if not tag.startswith('@')]

def url_base(url):
    return url.split('/')[-1]

def url_date(url, slice_len=3):
    return tuple(url.split('/')[-4:-4 + slice_len])

def get_dates(posts, slice_len):
    return {url_date(item['url'], slice_len) for item in posts['items']}

def date_posts(posts, *date):
    return [item for item in posts['items']
            if url_date(item['url'], len(date)) == date]

def get_archive_dates(posts):
    years = {}

    for item in posts['items']:
        year, month, day = url_date(item['url'])
        if year not in years:
            years[year] = {}
        if month not in years[year]:
            years[year][month] = {}
        if day not in years[year][month]:
            years[year][month][day] = 0
        years[year][month][day] += 1

    def get_cal(year, month):
        days = calendar.Calendar().monthdayscalendar
        return [[(year + '/' + month + '/' + '%02d' % day + '/',
                  '%02d' % day,
                  years[year][month].get('%02d' % day),
                  day)
                 for day in week]
                for week in days(int(year), int(month))]

    return [(year + '/', year,
             sum(sum(days.values()) for days in months.values()),
             [(year + '/' + month + '/', month,
               sum(days.values()),
               get_cal(year, month),
               datetime(int(year), int(month), 1).strftime('%B'))
              for month, days in sorted(months.items())])
            for year, months in sorted(years.items())]

def posts_view(src, **kwargs):
    view = src.copy()
    view.update(kwargs)
    return view

def paginated(posts):
    for page, filename in posts:
        for chunk, num, has_next in pages(page['posts']['items']):
            yield (posts_view(page,
                              base_filename=os.path.splitext(filename)[0],
                              posts=posts_view(page['posts'], items=chunk),
                              prev_page=suffix(filename, num - 1),
                              next_page=(suffix(filename, num + 1)
                                         if has_next else ''),
                              blog_url=('index.html' if num else '')),
                   suffix(filename, num) + '.html')

def pages(items, count=25):
    num = 0
    while items:
        chunk, items = items[:count], items[count:]
        yield chunk, num, bool(items)
        num += 1

def suffix(filename, p):
    if p < 0:
        return ''
    elif p == 0:
        return os.path.splitext(filename)[0]
    else:
        filename = os.path.splitext(filename)[0]
        return '{}-{}'.format(filename, p)

def get_pages(posts):
    # TODO: add script to add annotation count for each post to index
    # TODO: highlight annotations for each post on index
    yield {'posts': posts, 'has_rss': True}, 'index.html'

    # will still generate @user tag pages, even though @user tags won't be
    # displayed on the pages
    for tag in get_tags(posts):
        yield ({'posts': posts_view(posts, items=tag_posts(posts, tag)),
                'base_url': '../',
                'section': 'Posts tagged with',
                'section_tag': tag,
                'current_tags': [tag],
                'has_rss': True},
               'tags/' + tag + '.html')

    for year, in get_dates(posts, 1):
        yield ({'posts': posts_view(posts, items=date_posts(posts, year)),
                'base_url': '../',
                'section': 'Posts for ' + year,
                'current_year': year},
               year + '/index.html')

    for year, month in get_dates(posts, 2):
        items = date_posts(posts, year, month)
        yield ({'posts': posts_view(posts, items=items),
                'base_url': '../../',
                'section': 'Posts for ' + year + '/' + month,
                'current_year': year,
                'current_month': month},
               year + '/' + month + '/index.html')

    for year, month, day in get_dates(posts, 3):
        items = date_posts(posts, year, month, day)
        yield ({'posts': posts_view(posts, items=items),
                'base_url': '../../../',
                'section': 'Posts for ' + year + '/' + month + '/' + day,
                'current_year': year,
                'current_month': month,
                'current_day': day},
               year + '/' + month + '/' + day + '/index.html')

    for item in posts['items']:
        year, month, day = url_date(item['url'])
        yield ({'posts': posts_view(posts, items=[item]),
                'annotate': True,
                'base_url': '../../../',
                'current_year': year,
                'current_month': month,
                'current_day': day,
                'current_tags': item.get('tags', [])},
               relative_url(item['url'], posts) + '.html')

class DatetimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)

def relative_url(url, posts):
    return url.replace(posts['home_page_url'], '')

def make_relative_url(item, key, posts):
    if key in item:
        item[key] = relative_url(item[key], posts)

def html_view(page):
    page = deepcopy(page)

    for item in page['posts']['items']:
        make_relative_url(item, 'url', page['posts'])
        make_relative_url(item, 'image', page['posts'])
        make_relative_url(item, '_image_thumb', page['posts'])

        detect_video(item)

        attachments = item.get('attachments', [])
        item['attachments'] = []
        for attachment in attachments:
            # extract out cache
            if attachment.get('title') == 'cached':
                item['cached'] = {'url': relative_url(attachment['url'],
                                                      page['posts']),
                                  'title': attachment.get('title', 'cached')}
            else:
                item['attachments'].append(attachment)

    return page

def detect_video(item):
    if 'www.youtube.com/watch' in item.get('external_url', ''):
        item['video'] = ('https://www.youtube.com/embed/'
                         + item['external_url'].split('?v=')[1])

    if 'www.ted.com/talks' in item.get('external_url', ''):
        item['video'] = item['external_url'].replace('/www.', '/embed.')

    if 'vimeo.com/' in item.get('external_url', ''):
        item['video'] = item['external_url'].replace('vimeo.com',
                                                     'player.vimeo.com/video')

def rss_view(page):
    rss_posts = page['posts'].copy()
    if 'section' in page:
        rss_posts['title'] += ' - ' + page['section']
    if 'section_tag' in page:
        rss_posts['title'] += ' ' + page['section_tag']
    return rss_posts

def render(template, **kwargs):
    return Template(filename=template, input_encoding='utf-8',
                    imports=['from mako_filters import clean']) \
        .render_unicode(**kwargs)

def build_index(template, posts, base_url='', **page_data):
    page_data.update(posts)  # for python 3.4
    return render(template, base_url=base_url, **page_data)

def build_rss(rss_posts, rss_filename, timestamp):
    base_url = rss_posts['home_page_url']
    page_url = rss_filename.replace('index', '')
    rss_posts['feed_url'] = base_url + rss_filename + '.rss'

    return build_index('templates/index.rss',
                       timestamp=timestamp,
                       posts=rss_posts,
                       page_url=base_url + page_url)

def build_json(rss_posts, rss_filename, next_page):
    base_url = rss_posts['home_page_url']
    rss_posts['feed_url'] = base_url + rss_filename + '.json'
    rss_posts['home_page_url'] \
        = base_url + rss_filename.replace('index', '')
    if next_page:
        rss_posts['next_url'] = base_url + next_page + '.json'
    return json.dumps(rss_posts, cls=DatetimeEncoder, sort_keys=True, indent=4)

def build(json_feed, timestamp, write):
    posts = process(json_feed)

    tags = [(url, tag, len(tag_posts(posts, tag)))
            for url, tag in sorted(public_tags(get_tags(posts)))]
    archive = get_archive_dates(posts)

    for page, filename in paginated(get_pages(posts)):
        page.setdefault('has_rss', False)
        page.setdefault('annotate', False)

        page.setdefault('current_year', None)
        page.setdefault('current_month', None)
        page.setdefault('current_day', None)

        page.setdefault('current_tags', [])

        write(build_index('templates/index.html',
                          blog_css=BLOG_CSS,
                          public_tags=public_tags,
                          tags=tags,
                          archive=archive,
                          **html_view(page)),
              filename)

        if page['has_rss']:
            rss_filename = os.path.splitext(filename)[0]
            if not page['prev_page']:
                # no paging of rss - just include latest items
                write(build_rss(rss_view(page), rss_filename, timestamp),
                      rss_filename + '.rss')
            write(build_json(rss_view(page), rss_filename,
                             page.get('next_page', '')),
                  rss_filename + '.json')

def make_output(timestamp):
    os.mkdir('output_' + timestamp)

    # can't overwrite symlink, so create a new one and rename
    os.symlink('output_' + timestamp, 'output_tmp')
    os.replace('output_tmp', 'output')

    os.symlink('output_' + timestamp, 'next_release_tmp')
    os.replace('next_release_tmp', 'next_release')

    copy_asset('public/css/normalize.css', 'css/')
    copy_asset('public/css/skeleton.css', 'css/')
    copy_asset('public/css/blog.css', 'css/', BLOG_CSS)
    copy_asset('public/img/feed-icon-14x14.png', 'img/')

    # add link to cache into output
    print('Linking output/cached')
    os.symlink('../' + os.environ['CACHE_DIR'], 'output/cached')

    # add link to content into output
    print('Linking output/content')
    os.symlink('../' + os.environ['CONTENT_DIR'] + 'content/', 'output/content')

def write_output(content, filename):
    print('Writing', filename)
    os.makedirs(os.path.dirname('output/' + filename), exist_ok=True)
    with open('output/' + filename, mode='w', encoding='utf-8') as output:
        output.write(content)

def copy_asset(src, dst, dst_name=''):
    print('Copying', src, dst_name)
    os.makedirs('output/' + dst, exist_ok=True)
    shutil.copy2(src, 'output/' + dst + dst_name)

def build_local():
    timestamp = datetime.now(dateutil.tz.tzlocal())
    make_output(timestamp.strftime('%Y%m%d%H%M%S'))
    build(json.load(open(os.environ['CONTENT_FILE'])), timestamp, write_output)

@contextlib.contextmanager
def quiet(enabled=False):
    if enabled:
        with open(os.devnull, 'w') as f:
            with contextlib.redirect_stdout(f):
                yield
    else:
        yield

if __name__ == '__main__':
    print('Generating blog...')
    with quiet('--quiet' in sys.argv):
        build_local()
    print('Done')
