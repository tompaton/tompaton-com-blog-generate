import bleach

def clean(text):
    return bleach.linkify(bleach.clean(text), callbacks=[])
